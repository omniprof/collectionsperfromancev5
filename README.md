#CollectionsPerformanceV5

To make the Dictionary.jar file available to Maven, please enter the following on the command line. This assumes that Maven is installed.

mvn install:install-file -Dfile=C:\dev\ConFoo\CollectionsPerformanceV5\Dictionary\dictionary.jar -DgroupId=com.butlerpress -DartifactId=dict -Dversion=1.0 -Dpackaging=jar

Change the -Dfile value to show the full path to where you stored the Dictionary.jar file.
