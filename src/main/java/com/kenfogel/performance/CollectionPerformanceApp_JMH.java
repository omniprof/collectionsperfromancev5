package com.kenfogel.performance;

import com.kenfogel.performance.loaders.MapTests;
import com.kenfogel.performance.loaders.SequenceTests;
import com.patotski.performance.utils.BenchmarkUtils;
import org.openjdk.jmh.runner.RunnerException;

/**
 * Run the JMH test using the Benchmark class courtesy of Viktar Patotski @xpvit
 * https://github.com/xp-vit/java-logging-performance-tests/tree/main/src/jmh/java/com/patotski/performance
 *
 * @author Ken Fogel
 * @version 5.0
 *
 */
public class CollectionPerformanceApp_JMH {

    public static void main(String[] args) throws InterruptedException, RunnerException, Exception {
        BenchmarkUtils.runBenchmark(SequenceTests.class);
        BenchmarkUtils.runBenchmark(MapTests.class);
    }

}
