package com.kenfogel.performance.loaders;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;

import com.butlerpress.dict.Dictionary;
import com.kenfogel.performance.models.MapSpeedTableModel;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

/**
 * Rewritten to be able to do the benchmark internally for the Swing display or
 * by using Java Microbenchmark Harness
 * 
 * Performs a set of tests to determine the Big-O performance of an HashMap and
 * TreeMap
 *
 * Does multiple REPETITIONS on data structures of a given SIZE
 *
 * Displays the results in a table
 *
 * Uses Dictionary.jar Copyright (c) 2004-2007 Scott Willson that generates
 * random words from a set of words derived from a public source dictionary. See
 * Dictionary.jar for more information
 *
 * @author Ken Fogel
 * @version 5.0
 *
 */
@State(Scope.Benchmark)
public class MapTests {

    //private final static int REPETITIONS = 1000;
    private final static int SIZE = 1000;
    private final static int SEARCH_SIZE = 10;

    private final MapSpeedTableModel mapSpeedTableModel;
    private Set<String> dataSet;
    private Set<String> searchSet;
    private String[] dataArray;
    private String string;

    private final Random random;

    private long startTime, endTime;
    private HashMap<String, String> hashMap0;
    private HashMap<String, String> hashMap1;
    private HashMap<String, String> hashMap2;
    private HashMap<String, String> hashMap3;
    private TreeMap<String, String> treeMap1;
    private TreeMap<String, String> treeMap2;
    private TreeMap<String, String> treeMap3;

    /**
     * Constructor Receives reference to the table model that will hold the
     * results
     *
     * @param mapSpeedTableModel
     */
    public MapTests(MapSpeedTableModel mapSpeedTableModel) {
        this.mapSpeedTableModel = mapSpeedTableModel;
        random = new Random();
        loadDataSet();
        loadSearchSet();
    }

    /**
     * Constructor for JMH tests
     */
    public MapTests() {
        mapSpeedTableModel = new MapSpeedTableModel();
        random = new Random();
        loadDataSet();
        loadSearchSet();
    }

    /**
     * Load a set with random values and copy the values to an array The array
     * is used to select a SEARCH_SIZE of random words to search for
     */
    private void loadDataSet() {
        // Load Array
        dataSet = new HashSet<>();
        dataArray = new String[SIZE];
        for (int x = 0; x < SIZE; ++x) {
            do { // This loop will repeat if the random word in not unique
                string = Dictionary.getRandomWordTermCommonNameOrConnector();
                dataArray[x] = string;
            } while (!dataSet.add(string));
        }
        hashMap0 = new HashMap<>();
        for (int x = 0; x < SIZE; ++x) {
            hashMap0.put(dataArray[x], dataArray[x]);
        }
        treeMap1 = new TreeMap<>();
        treeMap2 = new TreeMap<>(hashMap0);
        treeMap3 = new TreeMap<>(hashMap0);
        hashMap1 = new HashMap<>();
        hashMap2 = new HashMap<>(hashMap0);
        hashMap3 = new HashMap<>(hashMap0);
    }

    /**
     * Select SEARCH_SIZE random words from the array to use in the search test
     */
    @SuppressWarnings("empty-statement")
    private void loadSearchSet() {
        searchSet = new HashSet<>();
        for (int x = 0; x < SEARCH_SIZE; ++x) {
            while (!searchSet.add(dataArray[random.nextInt(SIZE)]));
        }
    }

    @Benchmark
    public void do01LoadHashMap() {
        startTime = System.nanoTime();
        // Load Data
            for (int x = 0; x < SIZE; ++x) {
                hashMap1.put(dataArray[x], dataArray[x]);
            }
        endTime = System.nanoTime() - startTime;
    }

    @Benchmark
    public void do02AddToHashMap() {
        startTime = System.nanoTime();
            
        hashMap2.put("KenF", "KenF");
        endTime = System.nanoTime() - startTime;
    }

    @Benchmark
    public void do03HashMapSearch() {
        startTime = System.nanoTime();
            Iterator<String> it = searchSet.iterator();
            while (it.hasNext()) {
                string = hashMap3.get(it.next());
            }
        endTime = System.nanoTime() - startTime;
    }

    /**
     * Carry out the tests on the hash map
     */
    public void doHashMapTests() {
        do01LoadHashMap();
        mapSpeedTableModel.setValueAt(endTime /*/ 1e+6 REPETITIONS*/, 0, 1);
        do02AddToHashMap();
        mapSpeedTableModel.setValueAt(endTime /*/ 1e+6 REPETITIONS*/, 1, 1);
        do03HashMapSearch();
        mapSpeedTableModel.setValueAt(endTime /*/ 1e+6 REPETITIONS*/, 2, 1);
    }

    @Benchmark
    public void do04LoadTreeMap() {
        startTime = System.nanoTime();
        // Load Tree Map
            for (int x = 0; x < SIZE; ++x) {
                treeMap1.put(dataArray[x], dataArray[x]);
            }
        endTime = System.nanoTime() - startTime;
    }

    @Benchmark
    public void do05AddToTreeMap() {
        startTime = System.nanoTime();
        // Add Tree Map
            treeMap2.put("kenF", "kenF");
        endTime = System.nanoTime() - startTime;
    }

    @Benchmark
    public void do06TreeMapSearch() {
        startTime = System.nanoTime();
        // Find SEARCH_SIZE elements
            Iterator<String> it = searchSet.iterator();
            while (it.hasNext()) {
                string = treeMap3.get(it.next());
            }
        endTime = System.nanoTime() - startTime;
    }

    /**
     * Carry out the tests on a tree map
     */
    public void doTreeMapTests() {
        do04LoadTreeMap();
        mapSpeedTableModel.setValueAt(endTime /*/ 1e+6 REPETITIONS*/, 0, 2);
        do05AddToTreeMap();
        mapSpeedTableModel.setValueAt(endTime /*/ 1e+6 REPETITIONS*/, 1, 2);
        do06TreeMapSearch();
        mapSpeedTableModel.setValueAt(endTime /*/ 1e+6 REPETITIONS*/, 2, 2);
        
    }
}
