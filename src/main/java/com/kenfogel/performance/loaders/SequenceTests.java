package com.kenfogel.performance.loaders;

import java.util.*;

import com.butlerpress.dict.Dictionary;
import com.kenfogel.performance.models.SequenceSpeedTableModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

/**
 * Rewritten to be able to do the benchmark internally for the Swing display or
 * by using Java Microbenchmark Harness
 *
 * Performs a set of tests to determine the Big-O performance of an array, array
 * list, array deque, and linked list. Does multiple REPETITIONS on data
 * structures of a given SIZE. Displays the results in a table
 *
 * Uses Dictionary.jar Copyright (c) 2004-2007 Scott Willson that generates
 * random words from a set of words derived from a public source dictionary. See
 * Dictionary.jar for more information
 *
 * Corrected error where I was using the copy constructor of the ArrayList.
 * Needed to be replaced with a for loop. Updated to use Java 1.8 syntax
 *
 * @author Ken Fogel
 * @version 5.0
 *
 */
@State(Scope.Benchmark)
public class SequenceTests {

    private final static int SIZE = 1000;
    private String[] dataArray;
    private String string;

    private SequenceSpeedTableModel sequenceSpeedTableModel = null;

    private String[] array1;
    private String[] array2;

    private List<String> arrayList0;
    private List<String> arrayList1;
    private List<String> arrayList2;
    private List<String> arrayList3;
    private List<String> arrayList4;
    private List<String> arrayList5;

    private ArrayDeque<String> arrayDeque1;
    private ArrayDeque<String> arrayDeque2;
    private ArrayDeque<String> arrayDeque3;
    private ArrayDeque<String> arrayDeque4;

    private LinkedList<String> linkedList1;
    private LinkedList<String> linkedList2;
    private LinkedList<String> linkedList3;
    private LinkedList<String> linkedList4;
    private LinkedList<String> linkedList5;

    private long startTime, endTime;

    private final int pos = SIZE / 2;

    private static final Logger LOG
            = LogManager.getLogger(SequenceTests.class);

    /**
     * Constructor Receives reference to the table model that will hold the
     * results
     *
     * @param sequenceSpeedTableModel
     */
    public SequenceTests(SequenceSpeedTableModel sequenceSpeedTableModel) {
        this.sequenceSpeedTableModel = sequenceSpeedTableModel;
        loadArray();
    }

    public SequenceTests() {
        this.sequenceSpeedTableModel = new SequenceSpeedTableModel();
        loadArray();
    }

    /**
     * Load words from the dictionary into a set to eliminate duplication and
     * then into an array
     */
    public final void loadArray() {

        // Load Array
        dataArray = new String[SIZE];
        array1 = new String[SIZE];
        array2 = new String[SIZE];
        arrayList0 = new ArrayList<>();
        arrayList1 = new ArrayList<>();
        arrayList2 = new ArrayList<>();
        arrayList3 = new ArrayList<>();
        arrayList4 = new ArrayList<>();
        arrayList5 = new ArrayList<>();
        linkedList1 = new LinkedList<>();

        HashSet<String> dataSet = new HashSet<>();
        for (int x = 0; x < SIZE; ++x) {
            do { // This loop will repeat if the random word in not unique
                string = Dictionary.getRandomWordTermCommonNameOrConnector();
            } while (!dataSet.add(string));
            dataArray[x] = string;
            array1[x] = string;
            arrayList0.add(string);
            arrayList2.add(string);
            arrayList3.add(string);
            arrayList4.add(string);
            arrayList5.add(string);
        }
        arrayDeque2 = new ArrayDeque<>(arrayList0);
        arrayDeque3 = new ArrayDeque<>(arrayList0);
        arrayDeque4 = new ArrayDeque<>(arrayList0);
        linkedList2 = new LinkedList<>(arrayList0);
        linkedList3 = new LinkedList<>(arrayList0);
        linkedList4 = new LinkedList<>(arrayList0);
        linkedList5 = new LinkedList<>(arrayList0);
    }

    @Benchmark
    public void do01LoadArray() {
        startTime = System.nanoTime();
        // Load Array
        array1 = new String[SIZE];
        for (int x = 0; x < SIZE; ++x) {
            array1[x] = dataArray[x];
        }
        endTime = System.nanoTime() - startTime;
    }

    @Benchmark
    public void do02AccessFirstElementArray() {
        startTime = System.nanoTime();
        // Test access first element
        string = array2[0];
        endTime = System.nanoTime() - startTime;
    }

    @Benchmark
    public void do03AccessLastElementArray() {
        startTime = System.nanoTime();
        // Test access last element
        string = array2[SIZE - 1];
        endTime = System.nanoTime() - startTime;
    }

    @Benchmark
    public void do04AccessMiddleElementArray() {
        startTime = System.nanoTime();
        // Test access middle element
        string = array2[SIZE / 2];
        endTime = System.nanoTime() - startTime;
    }

    /**
     * Perform tests on an array
     */
    public void doArrayTests() {

        do01LoadArray();
        sequenceSpeedTableModel.setValueAt(endTime /*/ 1e+6 REPETITIONS*/, 0, 1);
        do02AccessFirstElementArray();
        sequenceSpeedTableModel.setValueAt(endTime /*/ 1e+6 REPETITIONS*/, 1, 1);
        do03AccessLastElementArray();
        sequenceSpeedTableModel.setValueAt(endTime /*/ 1e+6 REPETITIONS*/, 2, 1);
        do04AccessMiddleElementArray();
        sequenceSpeedTableModel.setValueAt(endTime /*/ 1e+6 REPETITIONS*/, 3, 1);

        // Does not support insert at start
        sequenceSpeedTableModel.setValueAt(-1L, 4, 1);
        // Does not support insert at end
        sequenceSpeedTableModel.setValueAt(-1L, 5, 1);
        // Does not support insert at middle
        sequenceSpeedTableModel.setValueAt(-1L, 6, 1);

        //array1 = array2 = null;
    }

    @Benchmark
    public void do05LoadArrayList() {
        startTime = System.nanoTime();
        // Load ArrayList
        //arrayList1 = new ArrayList<>();
        for (int x = 0; x < SIZE; ++x) {
            arrayList1.add(dataArray[x]);
        }
        endTime = System.nanoTime() - startTime;
    }

    @Benchmark
    public void do06AccessFirstElementArrayList() {
        startTime = System.nanoTime();
        // Test access first element
        string = arrayList2.get(0);
        endTime = System.nanoTime() - startTime;
    }

    @Benchmark
    public void do07AccessLastElementArrayList() {
        startTime = System.nanoTime();

        // Test access last element
        string = arrayList2.get(SIZE - 1);
        endTime = System.nanoTime() - startTime;
    }

    @Benchmark
    public void do08AccessMiddleElementArrayList() {
        startTime = System.nanoTime();

        // Test access middle element
        string = arrayList2.get(pos);
        endTime = System.nanoTime() - startTime;
    }

    @Benchmark
    public void do09InsertFirstElementArrayList() {
        startTime = System.nanoTime();
        // Insert at start
        arrayList3.add(0, "Dawson College");
        endTime = System.nanoTime() - startTime;
    }

    @Benchmark
    public void do10InsertLastElementArrayList() {
        startTime = System.nanoTime();
        // Insert at end
        arrayList4.add("Dawson College");
        endTime = System.nanoTime() - startTime;
    }

    @Benchmark
    public void do11InsertMiddleElementArrayList() {
        startTime = System.nanoTime();
        // Insert in middle
        arrayList5.add(pos, "Dawson College");
        endTime = System.nanoTime() - startTime;
    }

    /**
     * Perform tests on an ArrayList
     */
    public void doArrayListTests() {
        do05LoadArrayList();
        sequenceSpeedTableModel.setValueAt(endTime /*/ 1e+6 REPETITIONS*/, 0, 2);
        do06AccessFirstElementArrayList();
        sequenceSpeedTableModel.setValueAt(endTime /*/ 1e+6 REPETITIONS*/, 1, 2);
        do07AccessLastElementArrayList();
        sequenceSpeedTableModel.setValueAt(endTime /*/ 1e+6 REPETITIONS*/, 2, 2);
        do08AccessMiddleElementArrayList();
        sequenceSpeedTableModel.setValueAt(endTime /*/ 1e+6 REPETITIONS*/, 3, 2);
        do09InsertFirstElementArrayList();
        sequenceSpeedTableModel.setValueAt(endTime /*/ 1e+6 REPETITIONS*/, 4, 2);
        do10InsertLastElementArrayList();
        sequenceSpeedTableModel.setValueAt(endTime /*/ 1e+6 REPETITIONS*/, 5, 2);
        do11InsertMiddleElementArrayList();
        sequenceSpeedTableModel.setValueAt(endTime /*/ 1e+6 REPETITIONS*/, 6, 2);

        //arrayList0 = arrayList1 = arrayList2 = arrayList3 = arrayList4 = arrayList5 = null;
    }

    @Benchmark
    public void do12LoadDeque() {
        startTime = System.nanoTime();
        // Load ArrayDeque
        arrayDeque1 = new ArrayDeque<>();
        for (int x = 0; x < SIZE; ++x) {
            arrayDeque1.add(dataArray[x]);
        }
        endTime = System.nanoTime() - startTime;
    }

    @Benchmark
    public void do13AccessFirstElementDeque() {
        startTime = System.nanoTime();
        // Test access first element
        string = arrayDeque2.getFirst();
        endTime = System.nanoTime() - startTime;
    }

    @Benchmark
    public void do14AccessLastElementDeque() {
        startTime = System.nanoTime();
        // Test access last element
        string = arrayDeque2.getLast();
        endTime = System.nanoTime() - startTime;
    }

    @Benchmark
    public void do15InsertFirstElementDeque() {
        startTime = System.nanoTime();
        // Insert at start
        arrayDeque3 = new ArrayDeque<>(arrayList0);
        arrayDeque3.addFirst("Dawson College");
        endTime = System.nanoTime() - startTime;
    }

    @Benchmark
    public void do16InsertLastElementDeque() {
        startTime = System.nanoTime();
        // Insert at end
        arrayDeque4 = new ArrayDeque<>(arrayList0);
        arrayDeque4.addLast("Dawson College");
        endTime = System.nanoTime() - startTime;
    }

    /**
     * Perform tests on an ArrayDeque
     */
    public void doDequeTests() {

        do12LoadDeque();
        sequenceSpeedTableModel.setValueAt(endTime /*/ 1e+6 REPETITIONS*/, 0, 3);
        do13AccessFirstElementDeque();
        sequenceSpeedTableModel.setValueAt(endTime /*/ 1e+6 REPETITIONS*/, 1, 3);
        do14AccessLastElementDeque();
        sequenceSpeedTableModel.setValueAt(endTime /*/ 1e+6 REPETITIONS*/, 2, 3);
        // Access middle element not supported
        sequenceSpeedTableModel.setValueAt(-1L, 3, 3);
        do15InsertFirstElementDeque();
        sequenceSpeedTableModel.setValueAt(endTime /*/ 1e+6 REPETITIONS*/, 4, 3);
        do16InsertLastElementDeque();
        sequenceSpeedTableModel.setValueAt(endTime /*/ 1e+6 REPETITIONS*/, 5, 3);

        // Does not support insert at middle
        sequenceSpeedTableModel.setValueAt(-1L, 6, 3);

        //arrayDeque1 = arrayDeque2 = arrayDeque3 = arrayDeque4 = null;
    }

    @Benchmark
    public void do17LoadLinkedList() {
        startTime = System.nanoTime();
        // Load LinkedList
        for (int x = 0; x < SIZE; ++x) {
            linkedList1.add(dataArray[x]);
        }
        endTime = System.nanoTime() - startTime;
    }

    @Benchmark
    public void do18AccessFirstElementLinkedList() {
        startTime = System.nanoTime();
        // Test access first element
        string = linkedList2.getFirst();
        endTime = System.nanoTime() - startTime;
    }

    @Benchmark
    public void do19AccessLastElementLinkedList() {
        startTime = System.nanoTime();
        // Test access last element
        string = linkedList2.getLast();
        endTime = System.nanoTime() - startTime;
    }

    @Benchmark
    public void do20AccessMiddleElementLinkedList() {
        startTime = System.nanoTime();
        // Test access middle element
        string = linkedList2.get(pos);
        endTime = System.nanoTime() - startTime;
    }

    @Benchmark
    public void do21InsertFirstElementLinkedList() {
        startTime = System.nanoTime();
        // Insert at start
        linkedList3 = new LinkedList<>(arrayList0);
        linkedList3.addFirst("Dawson College");
        endTime = System.nanoTime() - startTime;
    }

    @Benchmark
    public void do22InsertLastElementLinkedList() {
        startTime = System.nanoTime();
        // Insert at end
        linkedList4 = new LinkedList<>(arrayList0);
        linkedList4.addLast("Dawson College");
        endTime = System.nanoTime() - startTime;
    }

    @Benchmark
    public void do23InsertMiddleElementLinkedList() {
        startTime = System.nanoTime();
        // Insert in middle
        linkedList5 = new LinkedList<>(arrayList0);
        linkedList5.add(pos, "Dawson College");
        endTime = System.nanoTime() - startTime;
    }

    /**
     * Perform tests on a LinkedList
     */
    public void doLinkedListTests() {

        do17LoadLinkedList();
        sequenceSpeedTableModel.setValueAt(endTime /*/ 1e+6 REPETITIONS*/, 0, 4);
        do18AccessFirstElementLinkedList();
        sequenceSpeedTableModel.setValueAt(endTime /*/ 1e+6 REPETITIONS*/, 1, 4);
        do19AccessLastElementLinkedList();
        sequenceSpeedTableModel.setValueAt(endTime /*/ 1e+6 REPETITIONS*/, 2, 4);
        do20AccessMiddleElementLinkedList();
        sequenceSpeedTableModel.setValueAt(endTime /*/ 1e+6 REPETITIONS*/, 3, 4);
        do21InsertFirstElementLinkedList();
        sequenceSpeedTableModel.setValueAt(endTime /*/ 1e+6 REPETITIONS*/, 4, 4);
        do22InsertLastElementLinkedList();
        sequenceSpeedTableModel.setValueAt(endTime /*/ 1e+6 REPETITIONS*/, 5, 4);
        do23InsertMiddleElementLinkedList();
        sequenceSpeedTableModel.setValueAt(endTime /*/ 1e+6 REPETITIONS*/, 6, 4);

        //linkedList1 = linkedList2 = linkedList3 = linkedList4 = linkedList5 = null;
    }
}
